# WebApp URL
URL=http://APP_DOMAIN

# domain used to create alias
EMAIL_DOMAIN=MAIL_DOMAIN

# transactional email is sent from this email address
SUPPORT_EMAIL=support@MAIL_DOMAIN

# custom domain needs to point to these MX servers
EMAIL_SERVERS_WITH_PRIORITY=[(10, "APP_DOMAIN.")]

# By default, new aliases must end with ".{random_word}". This is to avoid a person taking all "nice" aliases.
# this option doesn't make sense in self-hosted. Set this variable to disable this option.
DISABLE_ALIAS_SUFFIX=1

# the DKIM private key used to compute DKIM-Signature
DKIM_PRIVATE_KEY_PATH=/dkim.key

# DB Connection
DB_URI=postgresql://POSTGRES_USER:POSTGRES_PASS@POSTGRES_HOST:POSTGRES_PORT/POSTGRES_DB

FLASK_SECRET=FLASK_SECRET

GNUPGHOME=/sl/pgp

LOCAL_FILE_UPLOAD=1

POSTFIX_SERVER=localhost