# postgres config
hosts = POSTGRES_HOST
user = POSTGRES_USER
password = POSTGRES_PASS
dbname = POSTGRES_DB

query = SELECT domain FROM custom_domain WHERE domain='%s' AND verified=true
    UNION SELECT '%s' WHERE '%s' = 'MAIL_DOMAIN' LIMIT 1;